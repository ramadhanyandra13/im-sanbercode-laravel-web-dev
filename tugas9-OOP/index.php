<?php
    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    //intansiasi objek
    $sheep = new Animal("shaun");
        echo "Name  : " .$sheep->name ."<br>";
        echo "Legs : " .$sheep->legs ."<br>";
        echo "Cold Blooded : " .$sheep->cold_blooded ."<br>";
    
    $kodok = new Frog("buduk");
    echo "Name  : " .$kodok->name ."<br>";
    echo "Legs : " .$kodok->legs ."<br>";
    echo "Cold Blooded : " .$kodok->cold_blooded ."<br>";
    echo "Jump : ". $kodok->Jump() . "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Name  : " .$sungokong->name ."<br>";
    echo "Nama : " .$sungokong->legs ."<br>";
    echo "Nama Hewan : " .$sungokong->cold_blooded ."<br>";
    echo "Yell : " . $sungokong->Yell();
?>
